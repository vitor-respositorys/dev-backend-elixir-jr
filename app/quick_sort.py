def partition(array_numbers, begin, end):
    """
    sorts the elements between start and end
        array_numbers(list): list of numbers to be sorted
        begin(int): sort start point
        end(int): ordination end point
    return: start(int): last start position
    """
    start = (begin - 1 )
    last_number = array_numbers[end]
 
    for index in range(begin, end):
        if   array_numbers[index] <= last_number:
 
            # increment index of smaller element
            start = start + 1
            array_numbers[start], array_numbers[index] = \
                array_numbers[index], array_numbers[start]
 
    array_numbers[start + 1], array_numbers[end] = \
        array_numbers[end], array_numbers[start + 1]
    return (start + 1)

def quick_sort(array_numbers:list, begin:int=0, end:int=None):
    """
    sort a list of numbers
    parameters: 
        array_numbers(list): list of numbers to be sorted
        begin(int): sort start point
        end(int): ordination end point
    """

    # Create an auxiliary stack
    if end == None:
        end = len(array_numbers) - 1
    size = end - begin + 1
    stack = [0] * (size)
 
    top = 0
    stack[top] = begin
    top = top + 1
    stack[top] = end
 
    while top >= 0:
 
        end = stack[top]
        top = top - 1
        l = stack[top]
        top = top - 1
 
        p = partition(array_numbers, begin, end)
 
        if p-1 > begin:
            top = top + 1
            stack[top] = l
            top = top + 1
            stack[top] = p - 1
 
        if p + 1 < end:
            top = top + 1
            stack[top] = p + 1
            top = top + 1
            stack[top] = end