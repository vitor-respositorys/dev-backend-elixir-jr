from fastapi.testclient import TestClient
from main import app
import json

client = TestClient(app)

def test_numbers():
    response = client.get("/numbers/")
    numbers = json.loads(response.content.decode())
    for i in range(len(numbers)):
        for j in range(i, len(numbers)):
            if numbers[j] < numbers[i]:
                assert False
    assert True

def test_numbers_index():
    page = 100
    response = client.get(f"/numbers/{page}?order=true")
    numbers = json.loads(response.content.decode())
    for i in range(len(numbers)):
        for j in range(i, len(numbers)):
            if numbers[j] < numbers[i]:
                assert False
    assert True