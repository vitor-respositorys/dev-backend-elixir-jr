import random
from fastapi.testclient import TestClient
from main import app
from app.quick_sort import quick_sort


client = TestClient(app)


def test_quicksort_random():
    numbers = []
    for _ in range(1000):
        numbers.append(random.random())
    numbers_ordered = sorted(numbers)
    quick_sort(numbers)
    assert numbers == numbers_ordered

def test_quicksort_sequence_numbers():
    numbers = []
    limit = 1000
    for i in range(limit):
        numbers.append(i)
    numbers_ordered = sorted(numbers)
    quick_sort(numbers)
    assert numbers == numbers_ordered

def test_quicksort_inverse_sequence_numbers():
    numbers = []
    limit = 1000
    for i in range(limit):
        numbers.append(limit - i)
    numbers_ordered = sorted(numbers)
    quick_sort(numbers)
    assert numbers == numbers_ordered