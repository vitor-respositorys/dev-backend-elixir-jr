import requests
import json

def get_numbers_in_api(index: int) -> list:
    """
    get data from a page
    parameters: index indice of page
    return: a list with numbers returned presente on that page
    """
    url = f'http://challenge.dienekes.com.br/api/numbers?page={index}'
    response = requests.get(url)
    return json.loads(response.content.decode()).get('numbers')

def read_all_numbers(number=None) -> list:
    """
    read all numbers present in api
    return: all numbers present in api
    """
    index = number if number else 1
    result = get_numbers_in_api(index)
    numbers = []
    while(result != []):
        if result:
            numbers.extend(result)    
            index += 1
        result = get_numbers_in_api(index)
    return numbers