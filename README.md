# Resumo aplicação

Aplicação consiste em uma API REST com dois endpoints:

`/numbers` onde é possível obter todos os números de forma ordenada e não ordenada

`/numbers/{index}` onde é possível obter os números de um pagina em especifico também com opção ordenada e não ordenada


# Como executar em container

## Pré-requisitos
Você precisará ter instalado o [Docker](https://docs.docker.com/engine/install/) e o [Docker Compose](https://docs.docker.com/compose/install/).

## Executando
Com Docker e Docker Compose instalados, basta clonar este repositório, e dentro de sua pasta principal, rodar:

```bash
  docker-compose up --build
```

# como Executar em ambiente local

crie um ambiente virtual e inicialize o ambiente virtual como descrito em [Virtualenv Python](https://www.treinaweb.com.br/blog/criando-ambientes-virtuais-para-projetos-python-com-o-virtualenv)

Instale as dependencias do projeto através do comando abaixo:

```bash
pip install -r requirements.txt
```

para unicializar a aplicação rode o comando abaixo:

```bash
uvicorn main:app
```

e acesso o link [http://127.0.0.1:8000](http://127.0.0.1:8000)

## como executar os testes

para executar o testes é necessário ir até a pasta app do projeto

```bash
cd app
```

lembrando que deve estar com o ambiente virtual inicializado execute o comando abaixo dentro da pasta app
e todos os teste automáticos serão executados.

```bash
pytest
```

Existe um problema para executar os testes que é a demora para carregar todas as paginas
caso queira executar parcialmente basta modificar a linha 6 do arquivo `main.py` para

```bash
response_numbers = read_all_numbers(9990)
```
# Comentarios sobre o desafio

Ao implementar o teste `test_quicksort_sequence_numbers` ocorreu o erro `RecursionError` que mostrou a necessidade de implementar
uma versão iterativa do algoritmo de quick sort para que não ocorra erro de excesso de recusão esse erro não havia sido notado antes pois
o pior caso para versão implementada do algoritmo é quando os numeros estão em sequência. O erro antes não havia ocorrido nem mesmo ao consumir
todas as 10000 paginas da API.
