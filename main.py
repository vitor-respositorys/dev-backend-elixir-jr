from fastapi import FastAPI, status, HTTPException
from fastapi.responses import RedirectResponse
from app.extract_data import read_all_numbers, get_numbers_in_api
from app.quick_sort import quick_sort
app = FastAPI()
response_numbers = read_all_numbers()

@app.get("/", response_class=RedirectResponse)
def docs_redirect():
    response = RedirectResponse(url='/docs')
    return response

@app.get("/numbers", status_code=status.HTTP_200_OK, response_model=list, tags=["Numbers"])
def get_numbers(order:bool=True):
    """get all numbers present in system"""
    numbers = response_numbers.copy()
    if order:
        quick_sort(numbers)
    return numbers

@app.get("/numbers/{index}", status_code=status.HTTP_200_OK, response_model=list, tags=["Numbers"])
def get_numbers(index:int, order:bool=False):
    """get numbers present in a page"""
    numbers = get_numbers_in_api(index)
    if numbers:
        if order:
            quick_sort(numbers)
        return numbers
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)